## [1.2.1] - DEVELOPMENT
- Fixed undo when placing objects.
- Added shortcuts to get a prototype for next or previous object from selection.
- Added option to disable selection randomization after placing a new object.

## [1.2.0] - 2024-02-21
- Rework to preview real object when placing objects to be able to preview modifiers.

## [1.1.3]
- New shortcut to force regenerating random prototype when using place prefab tool. 
- Clone button to start placing new objects from scene objects (prefab instance or not).
- Support for scale while placing objects.
- Fixed get rotation and scale from picked object.

## [1.1.2] - 2023-02-21
- Improved support for setups with multiple scene views
- Improved behavior when the cursor leaves the scene view during rotation or changing the vertical offset

## [1.1.1] - 2022-01-08
- Added support for non-identity rotations in prefabs
- Added support for keeping the rotation of a picked-up object

## [1.1.0] - 2021-11-04
- Implemented support for Unity 2021.2's overlay system